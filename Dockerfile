FROM node:18-alpine
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install
RUN yarn add sequelize-cli
COPY . .
EXPOSE 8081
CMD yarn start
